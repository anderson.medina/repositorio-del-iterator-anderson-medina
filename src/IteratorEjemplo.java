import java.util.*;

public class IteratorEjemplo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add ("Juan");
        list.add ("Lucas");
        list.add ("Marta");

        Iterator<String> iterator = list.iterator ();
        while(iterator.hasNext()){
            String next=iterator.next();
            System.out.println(next);
        }

        Set<String> set = new HashSet<>();
        set.add ("Juan");
        set.add ("Lucas");
        set.add ("Marta");

        Iterator iterator2 = set.iterator ();
        while(iterator2.hasNext()){
            System.out.println(iterator2.next());
        }
    }

}
